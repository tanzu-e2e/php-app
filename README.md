# php-paketo-sample-app

## Build

pack build php-webserver-sample --builder paketobuildpacks/builder:full

## Run

docker run -it --env PORT=8080 -p 8080:8080 php-webserver-sample
